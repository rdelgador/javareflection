import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Interface1 
{
	public Class refClass = reflexion.class;
	public Package refPackage = refClass.getPackage();
	public Field[] refFields = refClass.getDeclaredFields();
	public Field refF;
	public Method[] refMethods = refClass.getDeclaredMethods();
	public Method refM;
	
	public Interface1(){};
	
	public void printField()
	{
		System.out.println("Lista de Campos");
		for(int i=0;i<refFields.length;i++)
		{
			refF = refFields[i];
			System.out.print(refF.getType());
			System.out.print(" ");
			System.out.println(refF.getName());
		}
		System.out.println("");
		
	}
	
	public void printMethod()
	{
		System.out.println("Lista de Metodos");
		for(int i=0;i<refMethods.length;i++)
		{
			refM = refMethods[i];
			System.out.print(refM.getReturnType());
			System.out.print(" ");
			System.out.print(refM.getName());
			System.out.print("( ) ");
			System.out.println("{ }");
		}
		
	}
	
	
}
